SERVICE_HELP_TARGETS += service-kibana-help

add-kibana: add-kibana-git

add-kibana-git:
	$(call SHOW_TARGET_EXEC, "загружаем скрипты для сервиса Kibana")
	@git clone git@gitlab.com:backing-service/services/kibana.git $(SERVICE_PATH)/kibana

service-kibana-help:
	$(call SHOW_CMD_HELP, add-kibana) добавить Kibana сервис
	$(call SHOW_CMD_HELP, rm-kibana) удалить Kibana сервис

rm-kibana:
	$(call SHOW_TARGET_EXEC, "удаляем скрипты сервиса Kibana")
	@rm -Rf $(SERVICE_PATH)/kibana
