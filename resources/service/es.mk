SERVICE_HELP_TARGETS += service-es-help

add-es: add-es-git

add-es-git:
	$(call SHOW_TARGET_EXEC, "загружаем скрипты для сервиса ElasticSearch")
	@git clone git@gitlab.com:backing-service/services/es.git $(SERVICE_PATH)/es

service-es-help:
	$(call SHOW_CMD_HELP, add-es) добавить ElasticSearch сервис
	$(call SHOW_CMD_HELP, rm-es) удалить ElasticSearch сервис

rm-es:
	$(call SHOW_TARGET_EXEC, "удаляем скрипты сервиса ElasticSearch")
	@rm -Rf $(SERVICE_PATH)/es
