SERVICE_HELP_TARGETS += service-redis-help
SERVICE_REDIS_PATH := $(SERVICE_PATH)/redis

add-redis: add-redis-git

add-redis-git:
	$(call SHOW_TARGET_EXEC, "загружаем скрипты для сервиса Redis")
	@git clone git@gitlab.com:backing-service/services/redis.git $(SERVICE_REDIS_PATH)

service-redis-help:
	$(call SHOW_CMD_HELP, add-redis) добавить Redis сервис
	$(call SHOW_CMD_HELP, rm-redis) удалить Redis сервис

rm-redis:
	$(call SHOW_TARGET_EXEC, "удаляем скрипты сервиса Redis")
	@rm -Rf $(SERVICE_REDIS_PATH)
