SERVICE_HELP_TARGETS += service-pgsql-help

add-pgsql: add-pgsql-git

add-pgsql-git:
	$(call SHOW_TARGET_EXEC, "загружаем скрипты для сервиса PostgresSQL")
	@git clone git@gitlab.com:backing-service/services/pgsql.git $(SERVICE_PATH)/pgsql

service-pgsql-help:
	$(call SHOW_CMD_HELP, add-pgsql) добавить PostgreSQL сервис
	$(call SHOW_CMD_HELP, rm-pgsql) удалить PostgreSQL сервис

rm-pgsql:
	$(call SHOW_TARGET_EXEC, "удаляем скрипты сервиса PostgreSQL")
	@rm -Rf $(SERVICE_PATH)/pgsql
