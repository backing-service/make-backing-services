SERVICE_HELP_TARGETS += service-mysql-help

add-mysql: add-mysql-git

add-mysql-git:
	$(call SHOW_TARGET_EXEC, "загружаем скрипты для сервиса MySQL")
	@git clone git@gitlab.com:backing-service/services/mysql.git $(SERVICE_PATH)/mysql

service-mysql-help:
	$(call SHOW_CMD_HELP, add-mysql) добавить MySQL сервис
	$(call SHOW_CMD_HELP, rm-mysql) удалить MySQL сервис

rm-mysql:
	$(call SHOW_TARGET_EXEC, "удаляем скрипты сервиса MySQL")
	@rm -Rf $(SERVICE_PATH)/mysql
