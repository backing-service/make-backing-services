SERVICE_HELP_TARGETS += service-adminer-help

add-adminer: add-adminer-git

add-adminer-git:
	$(call SHOW_TARGET_EXEC, "загружаем скрипты для сервиса Adminer")
	@git clone git@gitlab.com:backing-service/services/adminer.git $(SERVICE_PATH)/adminer

service-adminer-help:
	$(call SHOW_CMD_HELP, add-adminer) добавить Adminer сервис
	$(call SHOW_CMD_HELP, rm-adminer) удалить Adminer сервис

rm-adminer:
	$(call SHOW_TARGET_EXEC, "удаляем скрипты сервиса Adminer")
	@rm -Rf $(SERVICE_PATH)/adminer
