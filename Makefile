.DEFAULT_GOAL := help

TARGETS_HELP:=help
PRE_TARGETS_ENV:=pre-env
PRE_TARGETS_UP:=pre-up
PRE_TARGETS_START:=pre-start
PRE_TARGETS_STOP:=pre-stop
PRE_TARGETS_DOWN:=pre-down
PRE_TARGETS_DUMP:=pre-dump
PRE_TARGETS_RESTORE:=pre-restore

PROJECT_NAME?=MyAPP



-include .env
COMPOSE_PROJECT_NAME?=MyAPP
TARGETS_ENV?=$(PRE_TARGETS_ENV)

TARGETS_UP?=$(PRE_TARGETS_UP)
TARGETS_START?=$(PRE_TARGETS_START)
TARGETS_STOP?=$(PRE_TARGETS_STOP)
TARGETS_DOWN?=$(PRE_TARGETS_DOWN)
TARGETS_DUMP?=$(PRE_TARGETS_DUMP)
TARGETS_RESTORE?=$(PRE_TARGETS_RESTORE)

RESOURCES ?= resources
COMPOSE_FILE ?= docker-compose.yml
ENV ?= dev

COLOR_NC			= '\e[0m' # No Color
COLOR_WHITE			= '\e[1;37m'$(1)$(COLOR_NC)
COLOR_BLACK			= '\e[0;30m'$(1)$(COLOR_NC)
COLOR_BLUE			= '\e[0;34m'$(1)$(COLOR_NC)
COLOR_LIGHT_BLUE	= '\e[1;34m'$(1)$(COLOR_NC)
COLOR_GREEN			= '\e[0;32m'$(1)$(COLOR_NC)
COLOR_LIGHT_GREEN	= '\e[1;32m'$(1)$(COLOR_NC)
COLOR_CYAN			= '\e[0;36m'$(1)$(COLOR_NC)
COLOR_LIGHT_CYAN	= '\e[1;36m'$(1)$(COLOR_NC)
COLOR_RED			= '\e[0;31m'$(1)$(COLOR_NC)
COLOR_LIGHT_RED		= '\e[1;31m'$(1)$(COLOR_NC)
COLOR_PURPLE		= '\e[0;35m'$(1)$(COLOR_NC)
COLOR_LIGHT_PURPLE	= '\e[1;35m'$(1)$(COLOR_NC)
COLOR_BROWN 		= '\e[0;33m'$(1)$(COLOR_NC)
COLOR_YELLOW		= '\e[1;33m'$(1)$(COLOR_NC)
COLOR_GRAY			= '\e[0;30m'$(1)$(COLOR_NC)
COLOR_LIGHT_GRAY 	= '\e[0;37m'$(1)$(COLOR_NC)

BOLD=`tput bold`$(1)`tput sgr0`

SHOW_CMD_HELP = @echo $(call COLOR_YELLOW,$(call BOLD,make $(1))):
SHOW_TITLE_HELP = @echo "\t****** "$(call COLOR_RED,$(call BOLD, [ $(1) ]))" ******":
SHOW_TITLE_TARGET = @echo "\t****** "$(call COLOR_GREEN,$(call BOLD, [ $(1) ]))" ******":
SHOW_TARGET_EXEC = @echo "Выполняется:"$(call COLOR_YELLOW,$(call BOLD, $(1) ))
SHOW_TARGET_EXEC_END = @echo $(call COLOR_GREEN,$(call BOLD, [ $(1) ]))

#include resources/*.mk
include resources/project/Makefile
include resources/service/Makefile

#.PHONY: pgsql-up pgsql-down

help:
	$(call SHOW_TITLE_HELP, DEFAULT-HELP)
	$(call SHOW_CMD_HELP, help-all) справка по командам всех ресурсов
	$(call SHOW_CMD_HELP, init) выполнить инициализацию окружения
	$(call SHOW_CMD_HELP, up) поднять окружения
	$(call SHOW_CMD_HELP, restore) востанавливаем дампы и прочее для Developing окружения
	$(call SHOW_CMD_HELP, save) сохранить дампы и прочее для Developing окружения
	$(call SHOW_CMD_HELP, down) убрать окружение
	$(call SHOW_CMD_HELP, clean) очистить проект

help-all: $(TARGETS_HELP)

pre-env:
	@if [ $(ENV_PATH) != "." ]; then mkdir -p $(ENV_PATH); fi

env: $(TARGETS_ENV)

init: build-init
	# make dev-init - инициализация для DEV окружения делается отдельно(make dev-help)

pre-up:

pre-start:

pre-dump:

pre-restore:

pre-stop:

pre-down:

up: pre-up project-up

down: pre-down project-down

start: pre-start project-start

restore: $(TARGETS_RESTORE)

dump: $(TARGETS_DUMP)

clean:
	rm -Rf resources/services/*
	# make dev-clean - очистка для DEV окружения делается отдельно(make dev-help)


